-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           5.7.20-log - MySQL Community Server (GPL)
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Copiando estrutura do banco de dados para bd2
CREATE DATABASE IF NOT EXISTS `bd2` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `bd2`;

-- Copiando estrutura para tabela bd2.clube
CREATE TABLE IF NOT EXISTS `clube` (
  `NumClube` varchar(10) NOT NULL,
  `NomeClube` varchar(50) NOT NULL,
  `FinalidadeClube` varchar(50) NOT NULL,
  `OrcamentoClube` double DEFAULT NULL,
  `GastoRealClube` double DEFAULT NULL,
  PRIMARY KEY (`NumClube`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela bd2.clube: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `clube` DISABLE KEYS */;
INSERT INTO `clube` (`NumClube`, `NomeClube`, `FinalidadeClube`, `OrcamentoClube`, `GastoRealClube`) VALUES
	('C1', 'DELTA', 'SOCIAL', 1000, 1200),
	('C2', 'BITS', 'ACADEMIC', 500, 350),
	('C3', 'HELPS', 'SERVICE', 300, 330),
	('C4', 'SIGMA', 'SOCIAL', NULL, 150);
/*!40000 ALTER TABLE `clube` ENABLE KEYS */;

-- Copiando estrutura para tabela bd2.filiacao
CREATE TABLE IF NOT EXISTS `filiacao` (
  `NumAluno` varchar(10) NOT NULL,
  `NumClube` varchar(50) NOT NULL,
  PRIMARY KEY (`NumAluno`,`NumClube`),
  KEY `FK_filiacao_clube` (`NumClube`),
  CONSTRAINT `FK_filiacao_clube` FOREIGN KEY (`NumClube`) REFERENCES `clube` (`NumClube`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Copiando dados para a tabela bd2.filiacao: ~1 rows (aproximadamente)
/*!40000 ALTER TABLE `filiacao` DISABLE KEYS */;
INSERT INTO `filiacao` (`NumAluno`, `NumClube`) VALUES
	('A1', 'C1'),
	('A2', 'C1'),
	('A1', 'C2'),
	('A1', 'C3'),
	('A3', 'C3'),
	('A1', 'C4'),
	('A2', 'C4');
/*!40000 ALTER TABLE `filiacao` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;

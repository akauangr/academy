drop database if exists hospital;

create database hospital;

use hospital;

create table funcao(idfuncao int primary key, descricao varchar(100), acesso int);

insert into funcao values(1,'Paciente',1), (2,'Medico',2);

create table convenio (idconvenio int primary key, descricao varchar(100), telefone varchar(11));

insert into convenio values(1,'Unimed','11111111111'),
			   (2,'América','22222222222'),
                           (3,'SUS','33333333333');

create table tipologradouro (idtipologra int primary key, descricao varchar(50));

insert into tipologradouro values(1,'Rua'),
				 (2,'Avenida');

create table tipoexame (idexame int primary key, descricao varchar(100));

insert into tipoexame values(1,'Tomografia'),
			    (2,'Ressonância'),
                            (3,'Ultrassom'),
                            (4,'Exame de Sangue'),
                            (5,'Esteira'),
                            (6,'Eletrocardiograma');

create table tipoespecialidade (idtipoespecialidade int primary key, descricao varchar(100));

insert into tipoespecialidade values(1,'ClinicaGeral'),
				    (2,'Cardiologia'),
                                    (3,'Otorrinolaringologista'),
                                    (4,'Ginecologia'),
                                    (5,'Radiologia'),
                                    (6,'Cirurgia Geral'),
                                    (7,'Urologia');

create table endereco  (idendereco int primary key,
			idtipologra int,
			logradouro varchar(100),
			complement varchar(150),
			numero int,
			cep int,
			foreign key (idtipologra) references tipologradouro (idtipologra)
                       );
                       
insert into endereco values(1,1,'Rua A Setor A', 'Quadra A Lote A',23,74000000),
			   (2,2,'Avenida B Setor B', 'Quadra B Lote B',23,74000000);
                       
create table paciente ( idpaciente int primary key,
			idconvenio int,
                        nome varchar(100),
                        dtnascimento date,
                        idfuncao int,
                        telefone varchar(11),
                        celular varchar(11),
                        idendereco int,
                        foreign key (idconvenio) references convenio (idconvenio),
                        foreign key (idfuncao) references funcao (idfuncao),
                        foreign key (idendereco) references endereco (idendereco)
			);
                        
insert into paciente values(1,1,'Maria','1985-05-10',1,'11111111111','12222222222',1);

create table medico ( idmedico int primary key,
		      crm varchar(5),
                      uf varchar(2),
                      idespecialidade int,
                      nomemedico varchar(100),
                      dtnascimento date,
                      idfuncao int,
                      telefone varchar(11),
                      celular varchar(11),
                      idendereco int,
                      foreign key (idfuncao) references funcao(idfuncao),
                      foreign key (idendereco) references endereco(idendereco),
                      foreign key (idespecialidade) references tipoespecialidade(idtipoespecialidade)
		    );

insert into medico values(1,'2236','GO',6,'Adriano Teixeira Canedo','1974-04-20',2,'9845449850','11111111111',2),
			 (2,'1236','GO',5,'Carlos Fernando','1965-04-20',2,'96325417896','77777777777',2),
                         (3,'6325','GO',5,'Rogério Gonçalves','1962-04-20',2,'895623159','88888888888',2);

create table consultas (idconsultas int primary key,
			horario datetime,
                        idpaciente int,
                        idmedico int,
                        anotacoes varchar(1000),
			foreign key (idpaciente) references paciente(idpaciente),
			foreign key (idmedico) references medico(idmedico)
			);
                        
insert into consultas values(1,'2018-04-24',1,1,'Paciente com corisa, febre e com fraqueza');
                    
create table exames ( idexames int primary key,
			idmedicoresp int,
			idmedicosolic int,
			idpaciente int,
			idtipoexame int,
			anotacoes varchar(100),
			foreign key (idmedicoresp) references medico(idmedico),
			foreign key (idmedicosolic) references medico(idmedico),
			foreign key (idpaciente) references paciente(idpaciente),
			foreign key (idtipoexame) references tipoexame(idexame)
			);

insert into exames values(1,2,1,1,1,'A paciente se mostrou tranquila durante todo o procedimento');
